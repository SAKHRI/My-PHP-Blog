-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `contenu` text NOT NULL,
  `categorie` varchar(45) NOT NULL,
  `datetime_post` datetime DEFAULT NULL,
  `attachment` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (5,'Le PhÃ©nix','Comme le phÃ©nix qui pour renaÃ®tre a besoin de passer par le feu destructeur, j\'avais besoin d\'Ãªtre EN FEU pour me sentir libre et qu\'une part de moi-mÃªme tombÃ¢t en cendres comme dans quelque sacrifice paÃ¯en. Et ce dÃ©sir en moi de voler tel un oiseau pour dÃ©couvrir l\'infini. .. J\'ai prÃ©fÃ©rÃ© l\'intensitÃ© Ã  la longÃ©vitÃ©. Ma vie amoureuse aussi bien que mon art seraient rituellement sacrifiÃ©s sur l\'autel du feu Ã©ternel.','Lettre Ã  Marina - Niki de Saint Phalle',NULL,0),(10,'Le Scorpion','Lorsque nous Ã©voquons les animaux dangereux pour l\'homme, nous parlons volontiers du requin, du serpent, du scorpion ou de lâ€™araignÃ©e, voir de la mÃ©duse, et nous oublions Ã  chaque fois la femme.','Sugar Baby - Philippe Bartherotte',NULL,NULL),(11,'Le dragon','Ils ont la guivre, la licorne, la serpente, la salamandre, la tarasque, la drÃ©e, le dragon,l\'hippogriffe. Tout cela terreur pour nous, leur est ornement et parure. Ils ont une mÃ©nagerie qui s\'appelle le blason, et oÃ¹ rugissent les monstres inconnus.','L\'Homme qui rit (1869) de Victor Hugo',NULL,NULL),(12,'Le monstre','Quiconque lutte contre des monstres devrait prendre garde, dans le combat, Ã  ne pas devenir monstre lui-mÃªme. Et quant Ã  celui qui scrute le fond de l\'abysse, l\'abysse le scrute Ã  son tour.','Friedrich Nietzsche',NULL,NULL),(13,'Le tigre','Lorsqu\'un chat se tient Ã  l\'entrÃ©e du trou du rat, dix mille rats ne se hasardent pas Ã  en sortir; lorsqu\'un tigre garde le guÃ©, dix mille cerfs ne peuvent le traverser.','L\'art de la guerre de Sun Tzu',NULL,NULL);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 15:30:48
