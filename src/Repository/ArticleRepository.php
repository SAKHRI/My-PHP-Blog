<?php

namespace App\Repository;

use App\Entity\Article;



class ArticleRepository
{








    public function getAll() : array
    {

        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");
        $articles = [];
        try {
            /*On fait une instance de connexion à PDO en indiquant
            le lien de la base de donnée sql et son port (host). 
            Le nom de la base de donnée (dbname) qu'on veut cibler.
            Puis le nom d'utilisateur pour s'y connecter et son
            mot de passe
             */
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
            //On indique à PDO de déclencher des exceptions s'il a une erreur
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            /*
            La méthode prepare() sur une connexion PDO va mettre une
            requête SQL en attente d'exécution à l'intérieur d'un
            objet de type PDOStatement (qu'on stock ici dans $query)
             */
            $query = $cnx->prepare("SELECT * FROM article");
            //Pour la requête soit lancer, il faut lancer la méthode execute de l'objet query
            $query->execute();

            /*
            Pour récupérer les résultats de la requête, on peut 
            utiliser la méthode fetchAll qui renverra un tableau
            de tableau associatif. Chaque tableau associatif représentera une ligne de résultat.
            On fait une boucle sur le tableau de résultat pour
            faire en sorte de convertir chaque ligne de résultat
            brut en une instance de la classe voulue (ici SmallDog)
             */
            foreach ($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }

    public function add(Article $article)
    {
        /** connexion à PDO à externaliser dans une classe à
         * part (ou, dans une méthode au pire)
         */
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("INSERT INTO article (titre, url, contenu, categorie) VALUES (:titre, :url, :contenu, :categorie)");
            
            $query->bindValue(":titre", $article->titre);
            $query->bindValue(":url", $article->url);
            $query->bindValue(":contenu", $article->contenu);
            $query->bindValue(":categorie", $article->categorie);
            // $query->bindValue(":datetime_post", $article->datetime_post);
            // $query->bindValue(":attachment", $article->attachment);
            


            $query->execute();

            $article->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
    }


    public function update(Article $article)
    {



        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("UPDATE article SET titre= :titre, url= :url, contenu= :contenu, categorie= :categorie WHERE id= :id");


            $query->bindValue(":titre", $article->titre);
            $query->bindValue(":url", $article->url);
            $query->bindValue(":contenu", $article->contenu);
            $query->bindValue(":categorie", $article->categorie);
            // $query->bindValue(":datetime_post", $article->datetime_post);
            // $query->bindValue(":attachment", $article->attachment);
            $query->bindValue(":id", $article->id);



            return $query->execute();


        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id)
    {

        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("DELETE FROM article WHERE id= :id");


            $query->bindValue(":id", $id);


            return $query->execute();


        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function getById(int $id) : ?Article
    {

        try {

            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("SELECT * FROM article WHERE id= :id");


            $query->bindValue(":id", $id);


            $query->execute();

            $result = $query->fetchAll();

            if(count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }


        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }

}


