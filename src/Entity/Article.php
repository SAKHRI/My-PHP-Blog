<?php

namespace App\Entity;

class Article {
    public $id;
    public $titre;
    public $contenu;
    public $categorie;
    public $datetime_post;
    public $url;

    
    /**
     * Méthode qui assigne à l'instance de article les valeurs contenues
     * dans un tableau associatif d'une ligne de résultat pdo.
     */
    public function fromSQL(array $sql) {
        $this->id = $sql["id"];
        $this->titre = $sql["titre"];
        $this->contenu = $sql["contenu"];
        $this->categorie = $sql["categorie"];
        $this->datetime_post = $sql["datetime_post"];
        $this->url = $sql["url"];
    }
}
