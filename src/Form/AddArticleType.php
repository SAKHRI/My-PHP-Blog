<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\FileType;
/**
 * Plutôt que de créer le formulaire directement dans les controleurs
 * symfony préconise de créer des classes représentant les formulaire
 * appelées Type.
 * Ici, on fait une classe SmallDogType dans laquelle on définira 
 * les différents champs du formulaires ainsi que la classe à
 * laquelle le formulaire est liée
 */
class AddArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //On définit les champs ici
        $builder
            ->add('titre', TextType::class)
            ->add('url', FileType::class, [])
            ->add('contenu', TextareaType::class)
            ->add('categorie', TextType::class);
            // ->add('datetime_post', DateType::class);
            

            //On ne met pas les boutons submit histoire de rendre
            //le formulaire le plus réutilisable possible
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        //On indique quelle classe le formulaire permet de créer
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}
