<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Form\AddArticleType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\File;





/**
 * @Security("has_role('ROLE_ADMIN')")
 */

class ViewArticleController extends Controller


{
    /**
     * @Route("/article/update/{id}", name="view_article")
     */


    public function index(int $id, ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);
        $article->url = new File($article->url, false);

        $form = $this->createForm(AddArticleType::class, $article);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $fileName = $this->generateUniqueFileName().'.'.$article->url->guessExtension();
            $article->url->move(dirname(__FILE__)."/../../public/uploads", $fileName);
            $article->url = "uploads/".$fileName;
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }


        return $this->render('view_article/index.html.twig', [
            'form' => $form->createView(),
            "article" => $article,
        ]);
    }


    /**
     *  @Route("/article/remove/{id}", name="remove_article")
     */

    public function remove(int $id, ArticleRepository $repo)
    {

        $repo->delete($id);
        return $this->redirectToRoute("home");

    }

    /**
     *  @Route("/connect", name="connect")
     */

    public function connect(ArticleRepository $repo)
    {

        
        return $this->redirectToRoute("home");

    }

    private function generateUniqueFileName()
  {
      // md5() reduces the similarity of the file names generated by
      // uniqid(), which is based on timestamps
      return md5(uniqid());
  }
}