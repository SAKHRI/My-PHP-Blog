<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;

class SingleArticleController extends Controller
{
    /**
     * @Route("/article/{id}", name="single_article")
     */
    public function index(int $id, ArticleRepository $repo, Request $request)
    {

        $article = $repo->getById($id);

        return $this->render('single_article/index.html.twig', [
            "article" => $article,
        ]);

    }
}



